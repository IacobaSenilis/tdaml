#!/bin/sh
header=$(cd generics || exit; $1 -E - < tdaml.h | sed '/^[[:blank:]]*#/d;s/#.*//')
while read -r type; do
	type_snake=$(echo "$type" | tr ' ' _)
	echo "$header" | sed "s/(TYPE_SNAKE)/_$type_snake/g; s/TYPE_SNAKE/$type_snake/g; s/TYPE/$type/g" | tr "[:upper:]" "[:lower:]"
done < TYPES
