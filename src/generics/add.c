#include "include.h"

#define MAT_ADD(T) TOKENPASTE(mat_add_, T)

DECLARE_ADD {
	/*if ((x.x != y.x) || (x.y != y.y)) return 0;*/
	size_t i;
	PASTE_MAT(TYPE) c = MAT_NEW_EMPTY(TYPE)(a.x, a.y);
	for(i = 0; i < a.x*a.y; i++) {
		c.d[i] = a.d[i] + b.d[i];
	}
	return c;
}
