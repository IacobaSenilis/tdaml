#include "include.h"

DECLARE_NEW_EMPTY {
	PASTE_MAT(TYPE) z;
	z.x = x;
	z.y = y;
	z.d = malloc(z.x*z.y*sizeof(TYPE));
	return z;
}
