#include "include.h"

#define MAT_MUL(T) TOKENPASTE(mat_mul_, T)

DECLARE_MUL {
	size_t u;
	size_t i;
	size_t o;
	PASTE_MAT(TYPE) c = MAT_NEW_EMPTY(TYPE)(a.y, b.x);
	for(u = 0; u < a.y; u++) {
		for(i = 0; i < b.x; i++) {
			TYPE v = 0;
			for(o = 0; o < a.x; o++) {
				v += a.d[o+a.x*u]*b.d[b.x*o+i];
			}
			c.d[u*c.x+i] = v;
		}
	}
	return c;
}
