#include "include.h"

#define MAT_SUB(T) TOKENPASTE(mat_sub_, T)

DECLARE_SUB {
	/*if ((x.x != y.x) || (x.y != y.y)) return 0;*/
	size_t i;
	PASTE_MAT(TYPE) c = MAT_NEW_EMPTY(TYPE)(a.x, b.y);
	for(i = 0; i < a.x*a.y; i++) {
		c.d[i] = a.d[i] - b.d[i];
	}
	return c;
}
