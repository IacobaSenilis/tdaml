#include "include.h"

#define MAT_EXP(T) TOKENPASTE(mat_exp_, T)

DECLARE_EXP {
	size_t i;
	PASTE_MAT(TYPE) c = MAT_NEW_EMPTY(TYPE)(a.y, a.x);
	for(i = 0; i < a.x*a.y; i++) {
		c.d[i] = (TYPE) exp(a.d[i]);
	}
	return c;
}
