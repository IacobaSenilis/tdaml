#include "include.h"

#define MAT_TRANS(T) TOKENPASTE(mat_trans_, T)

DECLARE_TRANS {
	size_t i;
	size_t x;
	PASTE_MAT(TYPE) c = MAT_NEW_EMPTY(TYPE)(a.x, a.y);
	for(i = 0; i < c.x; i++) {
		for(x = 0; x < c.y; x++) {
			c.d[i+x*c.x] = a.d[x+i*a.x];
		}
	}
	return c;
}
