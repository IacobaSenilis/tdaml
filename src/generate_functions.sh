#!/bin/sh
for filename in ./generics/*.c; do
	file=$(echo "$filename" | awk -F "/" '{print $NF}')
	while read -r type; do
		type_snake=$(echo "$type" | tr ' ' _)
		out_file=$(printf "%s_%s" "$type_snake" "$file")
		sed "s/R1/$type_snake/g; s/R2/$type/g; s/R3/$file/g;" < TEMPLATE > "$out_file"
	done < TYPES
done
