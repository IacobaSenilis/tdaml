#!/bin/sh
echo "#include <stddef.h>"
while read -r type; do
	echo "typedef struct { $type * d; size_t x, y; } mat_$type;"
done < TYPES
