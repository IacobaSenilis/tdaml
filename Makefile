CONFIG = config.mk
include $(CONFIG)

all: build_static_lib build_dynamic_lib build_header

# Generate a c file for each function and type
build_generics:
	@cd src; ./generate_functions.sh

# Generate a header file containing struct definitions for each type
build_structs:
	@cd src; ./generate_structs.sh $(CC) > structs.h

build_function_defs:
	@cd src; ./generate_function_defs.sh $(CC) > functions.h

build_header: build_structs build_function_defs
	@cd src; cat structs.h functions.h > tdaml.h

# Compile all generated .c files to .o
compile: build_header
	@cd src; $(CC) $(CFLAGS) -c *.c

# Build a static library from .o files
build_static_lib: build_generics compile
	@cd src; $(AR) libtdaml.a *.o

# Build a dynamic library from .c files
build_dynamic_lib: build_generics
	@cd src; $(CC) $(CFLAGS) -fPIC -shared -o libtdaml.so *.c

# Clean up
clean:
	@cd src; ${RM} TEMPLATE_M; ${RM} *.c; ${RM} *.o; ${RM} *.a; ${RM} *.so; ${RM} *.h

# Install and Uninstall
install: all
	@cd src; cp libtdaml.* $(PREFIX)/lib/; cp tdaml.h $(PREFIX)/include/

uninstall:
	@cd $(PREFIX); ${RM} lib/libtdaml.a lib/libtdaml.so; ${RM} include/tdaml.h
